# note

`note` is a small shell script to be used from the command line:

```
note [noteName]
```

Run `note`, it accepts *one optional argument*, `noteName`.

The `name` will default to the *current date* if not passed as an argument.


## Installation

Run `make`.

That will add `note` as a symlink in `~/.local/bin/`.
```
ln -sf $(shell pwd)/note ~/.local/bin/
```

Be sure to be sure to have this folder in your `$PATH`.


## Config

To customize, edit/create the file `config` at `~/.config/note/config`.

Available variables, exemples values are used as *defaults*:
- `notesDir=~/notes/tmp/` The folder in which will be stored the notes you write using this application. If the provided path does not exist it will be created.

- `noteExtension=".txt"`
The extension `note-name.org` that your notes will use.

